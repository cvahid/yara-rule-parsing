# -*- coding: utf-8 -*-
"""ThreatConnect Playbook App"""

# Import default Playbook Class (Required)
from playbook_app import PlaybookApp
import plyara
import json

# pylint: disable=W0201
class App(PlaybookApp):
    """Playbook App"""

    def __init__(self, _tcex):
        """Initialize class properties.

        This method can be OPTIONALLY overridden.
        """
        super(App, self).__init__(_tcex)
        self.ruleset_parsed = []

    # def done(self):
    #     """Perform cleanup work before after App main logic."""
    #     self.tcex.log.debug('Running done.')

    def run(self):
        """Run the App main logic.

        This method should contain the core logic of the App.
        """
        # read inputs
        incoming_ruleset = self.tcex.playbook.read(self.args.ruleset)
        parser = plyara.Plyara()
        self.tcex.log.info('Processing String: {}'.format(incoming_ruleset))
        parsed_rules = parser.parse_string(incoming_ruleset)
        for item in parsed_rules:
            item.update({"rule_string": parser.rebuild_yara_rule(item)})
            self.ruleset_parsed.append(json.dumps(item))
        self.exit_message = 'Ruleset parsed.'

    # def start(self):
    #     """Perform prep work before running App main logic."""
    #     self.tcex.log.debug('Running start.')

    def write_output(self):
        """Write the Playbook output variables.

        This method should be overridden with the output variables defined in the install.json
        configuration file.
        """
        self.tcex.log.info('Writing Output')
        self.tcex.playbook.create_output('ruleset.parsed', self.ruleset_parsed)
